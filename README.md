# Bachelorprojekt - Klient/server-applikation til kvalitetskontrol af tandbehandlinger.

Bachelor's thesis by Joakim Ahnfelt-Rønne [ahnfelt@diku.dk], Jørgen Thorlund
Haahr [ormurin@diku.dk] and Ramón Soto Mathiesen [ramon@diku.dk] (June 2008)

# Abstract

A quality ensurance system for the "Danish Dental Association" has been de-
veloped. The solution consists of a client, a server and an administrator ap-
plication, which communicate with each other through a network. Forms can be
created for selected dental treatments. The system can receive submissions of
these forms from the client application. The client can then show a success
rate, which is the percentage of successful treatments, out of all the submitted
treatments.

The project fulfills all the requirements.
